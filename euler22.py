# -*- coding: utf-8 -*-
"""
Created on Thu Sep 26 17:35:02 2019

@author: Tim
"""

"""


Using names.txt (right click and 'Save Link/Target As...'), a 46K text file containing over five-thousand first names, begin by sorting it into alphabetical order. Then working out the alphabetical value for each name, multiply this value by its alphabetical position in the list to obtain a name score.

For example, when the list is sorted into alphabetical order, COLIN, which is worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the list. So, COLIN would obtain a score of 938 × 53 = 49714.

What is the total of all the name scores in the file?
"""



with open('names.txt','r') as f:
    data = f.readline();

names = [name.strip('"') for name in data.split(",")]
names = sorted(names)


#Making a function that counts the number of letters in a given word:

def count(word):
    value=0
    for letter in word:
        value+= ord(letter) - ord('A')+1
    return value

#making a function that calculates the total score of a word, including the multiplier
def score(index):
    name = names[index]
    value = count(name)
    total_score = value*(index+1)
    return total_score


#count the total score of every name
total_score = 0
for each_name in range(len(names)):
    total_score+=score(each_name)
print(total_score) 