# -*- coding: utf-8 -*-
"""
Created on Tue Sep 24 09:36:42 2019

@author: Tim
"""



"""


By starting at the top of the triangle below and moving to adjacent numbers on the row below, the maximum total from top to bottom is 23.

3
7 4
2 4 6
8 5 9 3

That is, 3 + 7 + 4 + 9 = 23.

Find the maximum total from top to bottom of the triangle below:

75
95 64
17 47 82
18 35 87 10
20 04 82 47 65
19 01 23 75 03 34
88 02 77 73 07 63 67
99 65 04 28 06 16 70 92
41 41 26 56 83 40 80 70 33
41 48 72 33 47 32 37 16 94 29
53 71 44 65 25 43 91 52 97 51 14
70 11 33 28 77 73 17 78 39 68 17 57
91 71 52 38 17 14 91 43 58 50 27 29 48
63 66 04 68 89 53 67 30 73 16 69 87 40 31
04 62 98 27 23 09 70 98 73 93 38 53 60 04 23

NOTE: As there are only 16384 routes, it is possible to solve this problem by trying every route. However, Problem 67, is the same challenge with a triangle containing one-hundred rows; it cannot be solved by brute force, and requires a clever method! ;o)
"""









triangle = """75
95 64
17 47 82
18 35 87 10
20 04 82 47 65
19 01 23 75 03 34
88 02 77 73 07 63 67
99 65 04 28 06 16 70 92
41 41 26 56 83 40 80 70 33
41 48 72 33 47 32 37 16 94 29
53 71 44 65 25 43 91 52 97 51 14
70 11 33 28 77 73 17 78 39 68 17 57
91 71 52 38 17 14 91 43 58 50 27 29 48
63 66 04 68 89 53 67 30 73 16 69 87 40 31
04 62 98 27 23 09 70 98 73 93 38 53 60 04 23"""


#converting into list
triangle = triangle.strip().split('\n')
#converting every list into number
for i in range(1,len(triangle)):
    triangle[i] = triangle[i].strip().split(' ')
    triangle[i] = [int(x) for x in triangle[i]]

triangle[0] = [75]

#function that finds max sum


#Now attempting a new method, the bottom up approach from dynamic programming
def find_max_sum():
    for rows in range(len(triangle)-1):
        for small_triangle in range(len(triangle[len(triangle)-2])):
            first_sum = triangle[len(triangle)-2][small_triangle]+triangle[len(triangle)-1][small_triangle]
            second_sum =  triangle[len(triangle)-2][small_triangle]+triangle[len(triangle)-1][small_triangle+1]
            #replaces bottom small 3 term-triangles with a single number which represents the max value already.
            if first_sum>=second_sum:
                triangle[len(triangle)-2][small_triangle] = first_sum
            else:
                triangle[len(triangle)-2][small_triangle] = second_sum
        #remove the final row and repeat this process until just 1 term is left
        triangle.remove(triangle[len(triangle)-1])
    return triangle
print(find_max_sum())

    
    