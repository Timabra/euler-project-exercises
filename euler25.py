# -*- coding: utf-8 -*-
"""
Created on Mon Sep 30 11:00:51 2019

@author: Tim
"""

"""


The Fibonacci sequence is defined by the recurrence relation:

    Fn = Fn−1 + Fn−2, where F1 = 1 and F2 = 1.

Hence the first 12 terms will be:

    F1 = 1
    F2 = 1
    F3 = 2
    F4 = 3
    F5 = 5
    F6 = 8
    F7 = 13
    F8 = 21
    F9 = 34
    F10 = 55
    F11 = 89
    F12 = 144

The 12th term, F12, is the first term to contain three digits.

What is the index of the first term in the Fibonacci sequence to contain 1000 digits?
"""


f=[]
#make an empty value, so that ith index corresponds to ith term in fibonacci sequence
f.append('None')
f.append(1)
f.append(1)

for i in range(1000000000):
    f.append(f[-1]+f[-2])
    #stop loop when 1000 digits long, print last one
    if len(str(f[-1]))>=1000:
        print(f.index(f[-1]))
        break
