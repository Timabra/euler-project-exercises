# -*- coding: utf-8 -*-
"""
Created on Thu Sep 26 13:30:56 2019

@author: Tim
"""


"""


n! means n × (n − 1) × ... × 3 × 2 × 1

For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.

Find the sum of the digits in the number 100!
"""



#factorial function
def factorial(number):
    result=1
    for i in range(1,number+1):
        result=result*i
    return result


#function that adds the digits in a number:
def adds_digits(number):
    string = str(number)
    total_sum = 0
    for letter in range(len(string)):
        total_sum+=int(string[letter])
    return total_sum


print(adds_digits(factorial(100)))