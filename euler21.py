# -*- coding: utf-8 -*-
"""
Created on Thu Sep 26 17:06:15 2019

@author: Tim
"""

"""


Let d(n) be defined as the sum of proper divisors of n (numbers less than n which divide evenly into n).
If d(a) = b and d(b) = a, where a ≠ b, then a and b are an amicable pair and each of a and b are called amicable numbers.

For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110; therefore d(220) = 284. The proper divisors of 284 are 1, 2, 4, 71 and 142; so d(284) = 220.

Evaluate the sum of all the amicable numbers under 10000.
"""
import numpy as np

#making a function that finds the set of numbers that divide evenly into our target
def proper_divisors(number):
    divisors = [1]
    for potential_factor in range(2,int(round(number/2)+1) ):
        if number%potential_factor==0:
            divisors.append(potential_factor)
    return divisors

#making a function that sums all of the divisors together:
def sum_divisors(set):
    return np.sum(set)


#making list of amicable pairs
amicable_set = []

#making a function that checks if given input a, d(a)=b is an amicable pair and if it is, add it to amicable_set:
def check_if_amicable_pair(a):
    amicable = False
    b = sum_divisors(proper_divisors(a))
    if sum_divisors(proper_divisors(b))==a and a!=b:
        amicable=True
        amicable_set.append(a)
        if b<=999:
            amicable_set.append(b)
    return


#repeat this for many values
for number in range(1,10001):
    check_if_amicable_pair(number)
#getting rid of duplicates:

amicable_set = list(dict.fromkeys(amicable_set))

#finding the sum of each pair
print(np.sum(amicable_set))