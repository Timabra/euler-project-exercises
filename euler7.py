# -*- coding: utf-8 -*-
"""
Created on Tue Sep 17 13:12:16 2019

@author: Tim
"""

primes=[2]

def xthprime(x):
    counter=0
    for i in range(1,100000000000000000):
       # print("i is", i)
        for j in range(2,int(round(i/2)+1)):
          #  print("j is", j)
            if i%j!=0:
                if j==int(round(i/2)):
                  #  print("APPENDING")
                    primes.append(i)
                    counter=counter+1
                    if counter==x:
                        return primes[x-1]
               # print("CONTINUING")
                continue
            else:
              #  print("BREAKING")
                break
print(xthprime(10001))