# -*- coding: utf-8 -*-
"""
Created on Mon Sep 30 11:13:59 2019

@author: Tim
"""

"""


A unit fraction contains 1 in the numerator. The decimal representation of the unit fractions with denominators 2 to 10 are given:

    1/2	= 	0.5
    1/3	= 	0.(3)
    1/4	= 	0.25
    1/5	= 	0.2
    1/6	= 	0.1(6)
    1/7	= 	0.(142857)
    1/8	= 	0.125
    1/9	= 	0.(1)
    1/10	= 	0.1 

Where 0.1(6) means 0.166666..., and has a 1-digit recurring cycle. It can be seen that 1/7 has a 6-digit recurring cycle.

Find the value of d < 1000 for which 1/d contains the longest recurring cycle in its decimal fraction part.
"""

#making a function that returns the fraction
def fraction(d):
    return 1/d

#now need to add all terms from 1 to 999 to a list:
    
fraction_list = []
 
for d in range(1,1000):
    fraction_list.append(fraction(d))
#print(fraction_list)

#function that finds the number of recurring digits

def recurring_digits(number):
    strings = str(number)
    #n is the number of recursive digits which we are checking for
    for n in range(1, 17):
        print("n is", n)
        #the following loop checks that the recursion is true throughout the loop
        for i in range(len(string)-2):
            print("i is", i)
            if string[2+i]==string[2+i+n]:
                if i==len(string)-3:
                    print(n)
                continue
            else:
                break
            
            
string = str(0.2345678901234567890)
print(string[17])




recurring_digits(0.3333333333333333333)
