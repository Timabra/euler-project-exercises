# -*- coding: utf-8 -*-
"""
Created on Tue Oct  1 11:29:31 2019

@author: Tim
"""

"""
Euler discovered the remarkable quadratic formula:

n2+n+41

It turns out that the formula will produce 40 primes for the consecutive integer values 0≤n≤39
. However, when n=40,402+40+41=40(40+1)+41 is divisible by 41, and certainly when n=41,412+41+41

is clearly divisible by 41.

The incredible formula n2−79n+1601
was discovered, which produces 80 primes for the consecutive values 0≤n≤79

. The product of the coefficients, −79 and 1601, is −126479.

Considering quadratics of the form:

    n2+an+b

, where |a|<1000 and |b|≤1000

where |n|
is the modulus/absolute value of n
e.g. |11|=11 and |−4|=4

Find the product of the coefficients, a
and b, for the quadratic expression that produces the maximum number of primes for consecutive values of n, starting with n=0."""
import numpy as np

#quadratic function:
def quadratic(a,b,n):
    return n**2+a*n + b

#function that checks if a number is prime
def isprime(number):
    if number<2:
        return False
    if number==2:
        return True
    for divisor in range(2,int(round(number**0.5+1))):
        if number%divisor==0:
            return False
        if divisor==int(round(number**0.5)):
            return True
    return False

#function that checks number of primes it gets right
def correct_primes(a,b):
    for n in range(1000):
        if isprime(quadratic(a,b,n)):
            continue
        else:
            #returns the number of terms for which this worked
            return n

print(np.arange(2,2))

#check all the a and b values
max_correct_primes = 0
for a in range(-1000,1000):
    for b in range(-1000,1000):
        number = correct_primes(a,b)
        if number>max_correct_primes:
            max_correct_primes = number
            print(a,b)
print(-61*971)
        
