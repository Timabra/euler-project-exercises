# -*- coding: utf-8 -*-
"""
Created on Fri Sep 20 12:30:09 2019

@author: Tim
"""
def function():
    trianglenumber=0
    for number in range(1000000):
        trianglenumber+=number
        factorcounter=0
        for factor in range (1, round(trianglenumber**0.5)+1):
            if trianglenumber%factor==0:
                if factor<round(trianglenumber**0.5):
                    factorcounter=factorcounter+2
                else:
                    factorcounter=factorcounter+1
                    print("This happened!!!")
        if factorcounter>500:
            print("FOUND ANSWER:", trianglenumber)
            return
        if int(number%100)==0:
            print("number is", number, "trianglenumber is", trianglenumber, "number of factors is", factorcounter)
function()