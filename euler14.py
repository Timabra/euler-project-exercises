# -*- coding: utf-8 -*-
"""
Created on Sun Sep 22 14:59:44 2019

@author: Tim
"""

def sequence(starting_number):
    number = starting_number
    counter=1
    for i in range(1000000):
        if number==1:
            return counter
        if number%2==0:
            number=number/2
            counter+=1
        else:
            number = 3*number+1
            counter+=1


def iterator(n):
    listofsequencelengths=[]
    for num in range(1,n):
        
        if int(num%10000)==0:
            print(num)
        listofsequencelengths.append(sequence(num))
    return listofsequencelengths

a = iterator(1000000)
print(a.index(max(a)))


#print(sequence(4))