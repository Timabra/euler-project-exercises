# -*- coding: utf-8 -*-
"""
Created on Mon Sep 23 10:55:38 2019

@author: Tim
"""


"""




If the numbers 1 to 5 are written out in words: one, two, three, four, five, then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.

If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many letters would be used?

NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and forty-two) contains 23 letters and 115 (one hundred and fifteen) contains 20 letters. The use of "and" when writing out numbers is in compliance with British usage.
"""




#this function counts the number of letters in any number below ten. Any number above 10 will be split into
#its part above and below ten later on.
def count_letters_0_to_19(number):
    if number==0:
        return 4
    if number==1:
        return 3
    if number==2:
        return 3
    if number==3:
        return 5
    if number==4:
        return 4
    if number==5:
        return 4
    if number==6:
        return 3
    if number==7:
        return 5
    if number==8:
        return 5
    if number==9:
        return 4
    if number==10:
        return 3
    if number==11:
        return 6
    if number==12:
        return 6
    if number==13:
        return 8
    if number==14:
        return 8
    if number==15:
        return 7
    if number==16:
        return 7
    if number==17:
        return 9
    if number==18:
        return 8
    if number==19:
        return 8
    print("SOMETHING WENT WRONG")
    
    
    
#Making a new function which counts the numbers of letters from 20 to 99. This function also uses the first function within it:
def count_letters_20_to_99(number):
    if number>=20 and number<40:
        #this accounts for the fact that one does n ot say twenty zero, simply just twenty:
        if number==20 or number==30:
            return 6
        #return 6 letters for twenty, plus the number of letters in the final digit
        return 6 + count_letters_0_to_19(number%10)
    
    
    #repeat same process for other numbers
    if number>=40 and number<70:
        if number==40 or number==50 or number==60:
            return 5
        return 5 + count_letters_0_to_19(number%10)


    if number>=70 and number<80:
        if number==70:
            return 7
        return 7 + count_letters_0_to_19(number%10)
    
    if number>=80 and number<100:
        if number==80 or number==90:
            return 6
        return 6 + count_letters_0_to_19(number%10)
    



#New function for numbers above 100. Uses previous functions within it:
def count_letters_100_to_1000(number):
    if number==100:
        return 10
    if number==1000:
        return 11
    number_of_hundreds = int(number/100-(number/100)%1)
    #number of letters in "hundred and":
    lettercounter=10
    #adding the number of hundreds which gives the first part, for example "one hundred and"
    lettercounter+=count_letters_0_to_19(number_of_hundreds)
    #adding final part after the "and..."
    remainder_from_100 = number%100
    if remainder_from_100<=19 and remainder_from_100>0:
        lettercounter+=count_letters_0_to_19(remainder_from_100)
    #accounting for the fact that 100,200,300... etc dont have an "and", they are just one hundred, two hundred etc.
    if remainder_from_100==0:
        lettercounter=lettercounter -3
    if remainder_from_100>19:

        lettercounter+=count_letters_20_to_99(remainder_from_100)
    return lettercounter




#now making the final function which can find the number of letters in any number from 0 to 1000:
def count_letters(number):
    if number>=0 and number<20:
        return count_letters_0_to_19(number)
    if number>=20 and number<100:
        return count_letters_20_to_99(number)
    if number>=100:
        return count_letters_100_to_1000(number)
    
"""    
#now scanning for max number of letters:
maxletters=0
for number in range(1000):
    num_of_letters = count_letters(number)
    if num_of_letters>=maxletters:
        maxletters=num_of_letters
        print("number is:",number, ",   max number of letters is", maxletters)
"""        
        
#adding all letters from 1 to 1000
counter=0
for i in range(1,1001):

    counter+=count_letters(i)
print(counter)







"""
#Returns the number of letters as the previous function if below 100. If above one hundred, also adds the number of letters
# in the first number + the number of letters in "hundred and" ie +10
def count_letters(number):
    if number>=10 and number<20:
        if number==10:
            return 3
        if number==11:
            return 6
        if number==12
        #splitting number into component greater than and smaller than one hundred (slicing)
        number = str(number)
        below_one_hundred = float(number[-2:])
        above_one_hundred = float(number[:-2])
        #print(below_one_hundred)
        lettercounter = count_letters_below_100(below_one_hundred) +count_letters_below_100(above_one_hundred)
        if above_one_hundred>=1:
            lettercounter+=10
        return lettercounter
print(count_letters(121))
  """    