# -*- coding: utf-8 -*-
"""
Created on Tue Sep 17 18:07:02 2019

@author: Tim
"""

summ = 0
primes=[]
for possibleprime in range(2,2000000):
    isprime=True
    for num in range(2, int(possibleprime**0.5)+1):
        if possibleprime%num==0:
            isprime=False
            break
    if isprime:
        primes.append(possibleprime)
        summ=summ+possibleprime
print(summ)