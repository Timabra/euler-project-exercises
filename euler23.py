# -*- coding: utf-8 -*-
"""
Created on Fri Sep 27 11:37:05 2019

@author: Tim
"""


"""
A perfect number is a number for which the sum of its proper divisors is exactly equal to the number. For example, the sum of the proper divisors of 28 would be 1 + 2 + 4 + 7 + 14 = 28, which means that 28 is a perfect number.

A number n is called deficient if the sum of its proper divisors is less than n and it is called abundant if this sum exceeds n.

As 12 is the smallest abundant number, 1 + 2 + 3 + 4 + 6 = 16, the smallest number that can be written as the sum of two abundant numbers is 24. By mathematical analysis, it can be shown that all integers greater than 28123 can be written as the sum of two abundant numbers. However, this upper limit cannot be reduced any further by analysis even though it is known that the greatest number that cannot be expressed as the sum of two abundant numbers is less than this limit.

Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers.
"""

import numpy as np

#Writing a function that finds out whether a number is abundant:
def abundant_check(number):
    abundant = False
    divisors = []
    for div in range(1,int(round(number/2)+1)):
        if number%div==0:
            divisors.append(div)
    divisors_sum = np.sum(divisors)
    if divisors_sum>number:
        abundant = True
    return abundant


print(abundant_check(13))

positive_integers = list(np.arange(2, 28124))
abundant_integers = []

for number in range(2,28124):
    if number%1000==0:
        print(number)
    if abundant_check(number)==True:
        abundant_integers.append(number)
print(abundant_integers)


for i in range(len(abundant_integers)):
    print(i)
    for j in range(i, len(abundant_integers)):

        abundant_sum = abundant_integers[i]+abundant_integers[j]
        if abundant_sum in positive_integers:
            positive_integers.remove(abundant_sum)
        if abundant_sum>28124:
            break

            
print(np.sum(positive_integers))
